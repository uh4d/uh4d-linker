import {
  Color, LineBasicMaterial, LineSegments, MathUtils,
  Matrix4, Mesh,
  MeshBasicMaterial,
  MeshDepthMaterial,
  Object3D,
  PerspectiveCamera,
  Scene,
  WebGLRenderer, WebGLRenderTarget
} from 'three';
import { JSDOM } from 'jsdom';
import { Random, MersenneTwister19937 } from 'random-js';
import { Presets, SingleBar } from 'cli-progress';
import { DateTime, Interval } from 'luxon';
import getopt from 'node-getopt';
import fs from 'fs-extra';
import gl from 'gl';
import got from 'got';
import Jimp from 'jimp';
import loadGltf from './gltf';
import { IDate, IImage, IObject } from './schemas';

global['document'] = new JSDOM().window.document; // eslint-disable-line
const baseUrl = 'https://h2890589.stratoserver.net';
// const baseUrl = 'http://localhost:3001';

const progressBar = new SingleBar({
  format: '{bar} {percentage}% | {value}/{total} | {message}'
}, Presets.shades_classic);

// getopt - parse parameters
const optInstance = getopt.create([
  ['s', 'scene=', 'Scene ID (required)'],
  ['i', 'image=', 'Process only specific image with ID'],
  ['r', 'render', 'Write render outputs to files (in output directory)'],
  ['', 'offset=', 'Offset to process only a section of queried images'],
  ['', 'length=', 'Length to process only a section of queried images']
]).bindHelp(`
Usage:
  npm start -- -s <scene id> [OPTION]

Options:
[[OPTIONS]]
`);
const opt = optInstance.parseSystem();

if (!opt.options.scene) {
  console.warn('No scene ID provided!');
  optInstance.showHelp();
  process.exit(1);
}

// init scene
const camera = new PerspectiveCamera(50, 1, 1, 1000);

const renderer = new WebGLRenderer({
  context: gl(400, 400),
  antialias: false,
  logarithmicDepthBuffer: true
});
renderer.setClearColor(0, 1.0);

const scene = new Scene();

// objects array, color map
const depthMat = new MeshDepthMaterial();
const colorMap = new Map<string, {color: Color, object: Object3D, material: MeshBasicMaterial}>();
const objects: Object3D[] = [];

const random = new Random(MersenneTwister19937.autoSeed());

// start pipeline
main()
  .then(() => {
    console.log('Done!');
  })
  .catch((err) => {
    console.error(err);
    process.exit(2);
  });

async function main(): Promise<void> {

  // load terrain
  console.log('Loading terrain...');

  await loadTerrain();

  // query images
  console.log('Query images...');
  const images: IImage[] = [];

  if (opt.options.image) {
    try {
      const response = await got<IImage>(`${baseUrl}/api/${opt.options.scene}/images/${opt.options.image}`, {
        responseType: 'json'
      });
      if (!response.body.date) {
        console.warn(`Image ${opt.options.image} has no date!`);
        process.exit(3);
      }
      if (!response.body.spatial) {
        console.warn(`Image ${opt.options.image} is not spatialized!`);
        process.exit(4);
      }
      images.push(response.body);
    } catch (e) {
      throw new Error(`Image ${opt.options.image} not found in database!`);
    }
  } else {
    const response = await got<IImage[]>(`${baseUrl}/api/${opt.options.scene}/images?q=spatial:set`, {
      responseType: 'json'
    });
    images.push(...response.body.filter((img) => !!img.date));
  }

  console.log('---------------');

  const offset = opt.options.offset ? parseInt(<string>opt.options.offset, 10) : 0;
  const length = opt.options.length ? parseInt(<string>opt.options.offset, 10) : images.length;

  console.log(`Process ${offset} to ${offset + length - 1} of ${images.length} images`);

  progressBar.start(length, 0, { message: 'N/A' });

  // iterate over images
  for (let i = offset, l = Math.min(images.length, offset + length); i < l; i++) {

    const img = images[i];

    const mediumDate = getMediumDate(img.date);
    progressBar.update({ message: `Current image: ${img.id}, Date: ${mediumDate}` });

    await queryObjects(mediumDate);
    const weights = await processImage(img);

    await got.post(`${baseUrl}/api/${opt.options.scene}/images/${img.id}/link`, {
      responseType: 'json',
      json: { weights }
    });

    progressBar.increment();

    if (global.gc) global.gc();

  }

  progressBar.update({ message: 'Finished' });
  progressBar.stop();

}

/**
 * Load terrain.
 */
async function loadTerrain(): Promise<void> {

  const gltf = await loadGltf(`${baseUrl}/data/terrain/${opt.options.scene}/dgm.gltf`);

  const terrain = gltf.scene.children[0];
  terrain.name = 'terrain';
  terrain.userData.colorMapKey = '000000';

  colorMap.set('000000', {
    color: new Color(0x000000),
    object: terrain,
    material: new MeshBasicMaterial({ color: 0x000000 })
  });

  scene.add(terrain);
  objects.push(terrain);

}

/**
 * Get the medium date of the image's date range.
 */
function getMediumDate(date: IDate): string {

  if (date.from === date.to) {
    return date.to;
  }

  const start = DateTime.fromISO(date.from);
  const end = DateTime.fromISO(date.to);
  const interval = Interval.fromDateTimes(start, end);
  const split = interval.divideEqually(2);

  return split[0].end.toISODate();

}

/**
 * Query objects by date and load them into scene if not yet present.
 */
async function queryObjects(date: string): Promise<void> {

  const response = await got<IObject[]>(`${baseUrl}/api/${opt.options.scene}/objects?date=${date}`, {
    responseType: 'json'
  });
  const toBeCreated: IObject[] = [];
  const toBeRemoved: Object3D[] = [];

  response.body.forEach((item) => {
    if (!objects.find((obj) => obj.userData.resource && obj.userData.resource.id === item.id)) {
      toBeCreated.push(item);
    }
  });

  objects.forEach((obj) => {
    if (!obj.userData.resource) return;
    if (!response.body.find((item) => obj.userData.resource.id === item.id)) {
      toBeRemoved.push(obj);
    }
  });

  toBeRemoved.forEach((obj) => {
    disposeObject(obj);
  });

  for (const data of toBeCreated) {
    const obj = await loadObject(data);
    objects.push(obj);
    scene.add(obj);
  }

}

/**
 * Load UH4D object, assign color (and update color map).
 */
async function loadObject(data: IObject): Promise<Object3D> {

  const url = `${baseUrl}/data/${data.file.path + data.file.file}`;
  const gltf = await loadGltf(url);

  const obj = gltf.scene.children[0];
  obj.userData.resource = data;

  const matrix = new Matrix4().fromArray(data.object.matrix);
  obj.applyMatrix4(matrix);
  obj.matrixAutoUpdate = false;

  // remove line segments
  const toBeRemoved: LineSegments[] = [];
  obj.traverse((child) => {
    if (child instanceof LineSegments) {
      toBeRemoved.push(child);
    }
  });
  toBeRemoved.forEach((child) => {
    if (child.parent) child.parent.remove(child);
    child.geometry.dispose();
    (child.material as LineBasicMaterial).dispose();
  });

  // set unique color
  let color;
  do {
    color = new Color(random.integer(1, 0xffffff));
  } while (colorMap.has(color.getHexString()));

  colorMap.set(color.getHexString(), {
    color,
    object: obj,
    material: new MeshBasicMaterial({ color })
  });
  obj.userData.colorMapKey = color.getHexString();

  return obj;

}

/**
 * Remove object from scene and array, delete color, dispose geometry.
 */
function disposeObject(obj: Object3D): void {

  const index = objects.indexOf(obj);

  if (index !== -1) {
    objects.splice(index, 1);
  }

  scene.remove(obj);

  obj.traverse((child) => {
    if (child instanceof Mesh) {
      child.geometry.dispose();
    }
  });

  if (colorMap.has(obj.userData.colorMapKey)) {
    colorMap.get(obj.userData.colorMapKey).material.dispose();
    colorMap.delete(obj.userData.colorMapKey);
  }

}

/**
 * Update camera and renderer according to image, render with color and depth materials,
 * and compute weights.
 */
async function processImage(img: IImage): Promise<{ [key: string]: number }> {

  const WIDTH = img.file.width;
  const HEIGHT = img.file.height;

  camera.fov = 2 * Math.atan(1 / (2 * img.spatial.ck)) * MathUtils.RAD2DEG;
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();

  camera.matrix.copy(new Matrix4());
  const matrix = new Matrix4().fromArray(img.spatial.matrix);
  camera.applyMatrix4(matrix);
  camera.matrixAutoUpdate = false;
  camera.updateMatrixWorld(true);

  // update renderer
  renderer.setSize(WIDTH, HEIGHT);
  const renderTarget = new WebGLRenderTarget(WIDTH, HEIGHT);
  renderer.setRenderTarget(renderTarget);

  // apply materials - color
  objects.forEach((obj) => {
    const mat = colorMap.get(obj.userData.colorMapKey).material;
    obj.traverse((child) => {
      if (child instanceof Mesh) {
        child.material = mat;
      }
    });
  });

  // render
  renderer.render(scene, camera);

  let buffer = new Uint8Array(WIDTH * HEIGHT * 4);
  renderer.readRenderTargetPixels(renderTarget, 0, 0, WIDTH, HEIGHT, buffer);

  const imageColor = new Jimp({
    data: buffer,
    width: WIDTH,
    height: HEIGHT
  });

  // apply materials - depth
  objects.forEach((obj) => {
    obj.traverse((child) => {
      if (child instanceof Mesh) {
        child.material = depthMat;
      }
    });
  });

  // render
  renderer.render(scene, camera);

  buffer = new Uint8Array(WIDTH * HEIGHT * 4);
  renderer.readRenderTargetPixels(renderTarget, 0, 0, WIDTH, HEIGHT, buffer);

  const imageDepth = new Jimp({
    data: buffer,
    width: WIDTH,
    height: HEIGHT
  });

  if (opt.options.render) {
    imageColor.flip(false, true);
    imageDepth.flip(false, true);

    await fs.ensureDir('output');
    imageColor.write(`output/${img.id}-color.png`);
    imageDepth.write(`output/${img.id}-depth.png`);
  }

  // compute weights
  const weightMap = new Map<string, number>();
  imageColor.scan(0, 0, WIDTH, HEIGHT, (x, y, i) => {
    const r = imageColor.bitmap.data[i];
    const g = imageColor.bitmap.data[i + 1];
    const b = imageColor.bitmap.data[i + 2];
    if (r === 0 && g === 0 && b === 0) {
      return;
    }
    const color = new Color(`rgb(${r},${g},${b})`);
    const key = color.getHexString();
    if (!weightMap.has(key)) {
      weightMap.set(key, 0);
    }
    weightMap.set(key, weightMap.get(key) + imageDepth.bitmap.data[i] / 255);
  });

  const objectWeights: { [key: string]: number } = {};
  for (const [key, value] of weightMap) {
    if (key !== '000000') {
      const { id } = colorMap.get(key).object.userData.resource;
      objectWeights[id] = value / (WIDTH * HEIGHT);
    }
  }

  return objectWeights;

}
