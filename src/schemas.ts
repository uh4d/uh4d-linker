export interface IObjectFile {
  id: string;
  path: string;
  file: string;
  original: string;
  type: string;
}

export interface IObject {
  id: string;
  name: string;
  date: {
    from: string;
    to: string;
  };
  object: {
    id: string;
    matrix: number[];
  };
  file: IObjectFile;
  misc: string;
  address: string;
  formerAddresses: string[];
  tags: string[];
}

export interface IImageFile {
  id: string;
  path: string;
  thumb: string;
  original: string;
  preview: string;
  type: string;
  width: number;
  height: number;
}

export interface IDate {
  from: string;
  to: string;
  value: string;
  display: string;
}

export interface ISpatial {
  id: string;
  ck: number;
  offset: [number, number];
  matrix: number[];
}

export interface IImage {
  id: string;
  title: string;
  description?: string;
  misc?: string;
  author?: string;
  owner?: string;
  permalink: string;
  captureNumber: string;
  date: IDate;
  file: IImageFile;
  spatial: ISpatial;
  spatialStatus: 0|1|2|3;
  tags: string[];
}
