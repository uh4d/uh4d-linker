import atob from 'atob';
import got from 'got';
import * as fs from 'fs-extra';
import { GLTF, GLTFLoader } from './GLTFLoader'; // eslint-disable-line
import { DRACOLoader } from './NodeDRACOLoader';

global.atob = atob;

const loader = new GLTFLoader();
loader.setDRACOLoader(new DRACOLoader());

/**
 * Load gltf file from server and decode/parse content.
 */
export default async function loadGltf(url: string): Promise<GLTF> {

  let buffer: Buffer;
  if (/^https?:\/\//.test(url)) {
    const response = await got(url, {
      responseType: 'buffer'
    });
    buffer = response.body;
  } else {
    buffer = await fs.readFile(url);
  }

  return new Promise((resolve, reject) => {

    loader.parse(buffer, '', (gltf) => {

      resolve(gltf);

    }, (err) => {

      reject(err);

    });

  });

}
