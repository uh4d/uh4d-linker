# uh4d-linker

Compute weights between images and objects and link them in database.

## Usage

Install dependencies:

    npm install

Build JS script from `.ts` files and execute via `npm start`.

    npm run build

```
Usage:
  npm start -- -s <scene id> [OPTION]

Options:
  -s, --scene= Scene ID (required)
  -i, --image= Process only specific image with ID
  -r, --render Write render outputs to files (in output directory)
  -h, --help   display this help
```

Alternatively, execute `.ts` files without building:

    npm run dev -- -s <scene id> [OPTION]
